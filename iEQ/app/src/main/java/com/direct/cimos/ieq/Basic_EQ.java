/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

/* ------------------------ CAN MESSAGE CONTROL ------------------------
 *
 *  event       = 1, 2, 3                     -> This basically selects which amp parameters you want to change.
 *  subEvent    = FADE_SEND, ... PRESET_READ  -> Above in this file you will find all the variations
 *  *data       = UINT8 data1[X] = {..., ...} -> This will be parameters you want to increase or decrease.
 *  length      = is the length of data.      -> I have actually not used this for reading and writing but this is more used for the Que_Message to ensure the maximum length of message sent.
 *
 *                    CAN message data base:
 *    __________________________________________________________________________
 *    | CAN ID | Byte0 | Byte1 | Byte2 | Byte3 | Byte4 | Byte5 | Byte6 | Byte7 |
 *    | 0x800  | 0x01  | Fade  |Balance| Bass  |  Mid  |Treble |  Sub  |unused |
 *    | Write  | 0x02  | Band  | Level |unused |unused |unused |unused |unused |
 *    |        | 0x03  |Preset |Staging|unused |unused |unused |unused |unused |
 *    __________________________________________________________________________
 *    | CAN ID | Byte0 | Byte1 | Byte2 | Byte3 | Byte4 | Byte5 | Byte6 | Byte7 |
 *    | 0x801  | 0x01  | Fade  |Balance| Bass  |  Mid  |Treble |  Sub  |unused |
 *    |  Read  | 0x02  | Band  | Level |unused |unused |unused |unused |unused |
 *    |        | 0x03  |Preset |Staging|unused |unused |unused |unused |unused |
 *    __________________________________________________________________________
 *
/ ------------------------- CAN MESSAGE CONTROL ----------------------*/


package com.direct.cimos.ieq;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;
import android.os.Handler;
import java.io.IOException;

public class Basic_EQ extends AppCompatActivity
{
// SUB-event id numbers
        private static final byte FADE_SEND             = (byte)0;
        private static final byte FADE_READ             = (byte)1;
        private static final byte BALANCE_SEND          = (byte)2;
        private static final byte BALANCE_READ          = (byte)3;
        private static final byte BASS_SEND             = (byte)4;
        private static final byte BASS_READ             = (byte)5;
        private static final byte MID_SEND              = (byte)6;
        private static final byte MID_READ              = (byte)7;
        private static final byte TREBLE_SEND           = (byte)8;
        private static final byte TREBLE_READ           = (byte)9;
        private static final byte SUB_SEND              = (byte)10;
        private static final byte SUB_READ              = (byte)11;
        private static final byte BAND_SEND             = (byte)12;    // Could start from 0 but...
        private static final byte BAND_READ             = (byte)13;
        private static final byte STAGGING_SEND         = (byte)14;    // Could start from 0 but...
        private static final byte STAGGING_READ         = (byte)15;
        private static final byte PRESET_SEND           = (byte)16;
        private static final byte PRESET_READ           = (byte)17;

    private static final int EXTRA_SHORT_DELAY = 300; // 3msecs
    private byte frame_1[];
    String address = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent from_main = getIntent();
        address = from_main.getStringExtra(iEQ.EXTRA_ADDRESS); //receive the address of the bluetooth device

        setContentView(R.layout.activity_basic__eq);


        //get_amp_settings();

    }

    // fast way to call Toast
    private void msg(String text)
    {
        final Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, EXTRA_SHORT_DELAY);
    }

    public void onSendClick(View v)
    {
        if(iEQ.btSocket.isConnected()) {
            try {
                iEQ.btSocket.getOutputStream().write(frame_1);
                msg("Sent");

            } catch (IOException e) {
                msg("Failed to send: Check your connection:");
            }
        }
    }

    public void toAdvancedEQ(View view)
    {
        if (iEQ.btSocket == null) {msg("Connect a Bluetooth device first:"); return;}
        Intent myIntent = new Intent(this, Advanced_EQ.class);
        startActivity(myIntent);
    }

    public void back (View veiw)
    {
        finish();
    }

//    @Override
//    protected void onDestroy()
//    {
//        super.onDestroy();
//        if (iEQ.btSocket.isConnected())
//        {
//            try { iEQ.btSocket.close(); }
//            catch (IOException e) {}
//        }
//    }


    public void send_to_m2_board(byte subEvent, byte type, byte data)       // read =
    {
        /*
         *    __________________________________________________________________________
         *    | CAN ID | Byte0 | Byte1 | Byte2 | Byte3 | Byte4 | Byte5 | Byte6 | Byte7 | Byte8 |
         *    | 0x800  | 0x01  | Fade  |Balance| Bass  |  Mid  |Treble |  Sub  |unused |
         *    | Write  | 0x02  | Band  | Level |unused |unused |unused |unused |unused |
         *    |        | 0x03  |Preset |Staging|unused |unused |unused |unused |unused |
         *    __________________________________________________________________________
         *
         *    // SUB-event id numbers
         * #define FADE_SEND             0
         * #define FADE_READ             1
         * #define BALANCE_SEND          2
         * #define BALANCE_READ          3
         * #define BASS_SEND             4
         * #define BASS_READ             5
         * #define MID_SEND              6
         * #define MID_READ              7
         * #define TREBLE_SEND           8
         * #define TREBLE_READ           9
         * #define SUB_SEND              10
         * #define SUB_READ              11
         * #define BAND_SEND             12    // Could start from 0 but...
         * #define BAND_READ             13
         * #define STAGGING_SEND         14    // Could start from 0 but...
         * #define STAGGING_READ         15
         * #define PRESET_SEND           16
         * #define PRESET_READ           17
         */
        frame_1[0] = 0x01;
        frame_1[1] = subEvent;
        frame_1[2] = data;

        //iEQ.btSocket.getInputStream().read();

        try { iEQ.btSocket.getOutputStream().write(frame_1);}
        catch (IOException e) {}
    }


    public void get_amp_settings()
    {
        byte buf[] = new byte[255];
        try
        {
            iEQ.btSocket.getOutputStream().write(0xDD00DD);
            iEQ.btSocket.getInputStream().read(buf);
        }

        catch (IOException e) {}



    }


    public void set_up_basic_eq()
    {
        frame_1 = new byte[8];

        SeekBar bass = (SeekBar)findViewById(R.id.seekBar);
        SeekBar mid = (SeekBar)findViewById(R.id.seekBar1);
        SeekBar treble = (SeekBar)findViewById(R.id.seekBar2);
        SeekBar subwoofer = (SeekBar)findViewById(R.id.seekBar3);
        SeekBar fade = (SeekBar)findViewById(R.id.seekBar4);
        SeekBar balance = (SeekBar)findViewById(R.id.seekBar5);

        bass.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                msg(Integer.toString(progress));
                send_to_m2_board(BASS_SEND, (byte)1,  (byte)(progress & (0xFF)));
            }
        });
        mid.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                msg(Integer.toString(progress));
                send_to_m2_board(MID_SEND, (byte)1,  (byte)(progress & (0xFF)));
            }
        });
        treble.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                msg(Integer.toString(progress));
                send_to_m2_board(TREBLE_SEND, (byte)1,  (byte)(progress & (0xFF)));
            }
        });
        subwoofer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                msg(Integer.toString(progress));
                send_to_m2_board(SUB_SEND, (byte)1,  (byte)(progress & (0xFF)));
            }
        });
        fade.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                msg(Integer.toString(progress));
                send_to_m2_board(FADE_SEND, (byte)1,  (byte)(progress & (0xFF)));
            }
        });
        balance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                msg(Integer.toString(progress));
                send_to_m2_board(BALANCE_SEND, (byte)1,  (byte)(progress & (0xFF)));
            }
        });
    }
}



