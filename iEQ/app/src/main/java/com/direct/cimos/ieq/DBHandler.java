/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

package com.direct.cimos.ieq;/**
 * Created by Cimos on 27/9/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "presets";
    // Contacts table name
    private static final String TABLE_SHOPS = "user ";
    // Shops Table Columns names
    private static final String KEY_ID = "_id";

    private static final String KEY_NAME  = " name";
    private static final String KEY_TAG  = " tag";
    private static final String KEY_BAND1 = " band_1";
    private static final String KEY_BAND2 = " band_2";
    private static final String KEY_BAND3 = " band_3";
    private static final String KEY_BAND4 = " band_4";
    private static final String KEY_BAND5 = " band_5";
    private static final String KEY_BAND6 = " band_6";
    private static final String KEY_BAND7 = " band_7";




    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_CONTACTS_TABLE =      "CREATE TABLE " + TABLE_SHOPS + "( " + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_NAME + " TEXT, " + KEY_TAG + " TEXT, " + KEY_BAND1 + " TEXT, " + KEY_BAND2 + " TEXT, " + KEY_BAND3 + " TEXT, " + KEY_BAND4 +
                " TEXT, " + KEY_BAND5 + " TEXT, " + KEY_BAND6 + " TEXT, " + KEY_BAND7 + " TEXT " + ")";

        /*+ " INTEGER, "
                                            +  KEY_BAND3 + " INTEGER, "+  KEY_BAND4 + " INTEGER, " +  KEY_BAND5 + " INTEGER, "
                                            +  KEY_BAND6 + " INTEGER, " +  KEY_BAND7 + " INTEGER"*/


        db.execSQL(CREATE_CONTACTS_TABLE);      // creates the database
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPS);
        // Creating tables again
        onCreate(db);
    }

    public void addPreset(Shop preset)
    {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, preset.getName()); // preset Name
        values.put(KEY_TAG, preset.getTag());
        values.put(KEY_BAND1, preset.getband(1));
        values.put(KEY_BAND2, preset.getband(2));
        values.put(KEY_BAND3, preset.getband(3));
        values.put(KEY_BAND4, preset.getband(4));
        values.put(KEY_BAND5, preset.getband(5));
        values.put(KEY_BAND6, preset.getband(6));
        values.put(KEY_BAND7, preset.getband(7));

        db.insert(TABLE_SHOPS, null, values);
        db.close(); // Closing database connection
    }

    public Shop getPreset(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SHOPS, new String[]{ KEY_ID,
                        KEY_NAME, KEY_TAG, KEY_BAND1, KEY_BAND2, KEY_BAND3,
                        KEY_BAND4, KEY_BAND5, KEY_BAND6, KEY_BAND7 }, KEY_ID + "=?",

                new String[] { String.valueOf(id) }, null, null, null, null);


        if (cursor != null)
            cursor.moveToFirst();

        //cursor.close();

        return new Shop(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)
                , cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9));
    }



    public List<String> getAllPresets()
    {
        List<String> preset_list = new ArrayList<String>();


        String selectQuery = "SELECT * FROM " + TABLE_SHOPS;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst())
        {
            do
            {
                Shop shop = new Shop();
                //shop.setId(Integer.parseInt(cursor.getString(0)));
                shop.setName(cursor.getString(1));
                preset_list.add(shop.getName());
            } while (cursor.moveToNext());
        }

        //cursor.close();
        return preset_list;
    }



    public int getPresetsCount()
    {
        String countQuery = "SELECT * FROM " + TABLE_SHOPS;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }



    public int updatePreset(Shop preset)
    {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, preset.getName());
        values.put(KEY_TAG, preset.getTag());

        values.put(KEY_BAND1, preset.getband(1));
        values.put(KEY_BAND2, preset.getband(2));
        values.put(KEY_BAND3, preset.getband(3));
        values.put(KEY_BAND4, preset.getband(4));
        values.put(KEY_BAND5, preset.getband(5));
        values.put(KEY_BAND6, preset.getband(6));
        values.put(KEY_BAND7, preset.getband(7));

        return db.update(TABLE_SHOPS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(preset.getId())});
    }



    public void deletePreset(Shop preset)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SHOPS, KEY_ID + " = ?",
                new String[] { String.valueOf(preset.getId()) });
        db.close();
    }
}