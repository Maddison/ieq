/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

/* ------------------------ CAN MESSAGE CONTROL ------------------------
 *
 *  event       = 1, 2, 3                     -> This basically selects which amp parameters you want to change.
 *  subEvent    = FADE_SEND, ... PRESET_READ  -> Above in this file you will find all the variations
 *  *data       = UINT8 data1[X] = {..., ...} -> This will be parameters you want to increase or decrease.
 *  length      = is the length of data.      -> I have actually not used this for reading and writing but this is more used for the Que_Message to ensure the maximum length of message sent.
 *
 *                    CAN message data base:
 *    __________________________________________________________________________
 *    | CAN ID | Byte0 | Byte1 | Byte2 | Byte3 | Byte4 | Byte5 | Byte6 | Byte7 |
 *    | 0x800  | 0x01  | Fade  |Balance| Bass  |  Mid  |Treble |  Sub  |unused |
 *    | Write  | 0x02  | Band  | Level |unused |unused |unused |unused |unused |
 *    |        | 0x03  |Preset |Staging|unused |unused |unused |unused |unused |
 *    __________________________________________________________________________
 *    | CAN ID | Byte0 | Byte1 | Byte2 | Byte3 | Byte4 | Byte5 | Byte6 | Byte7 |
 *    | 0x801  | 0x01  | Fade  |Balance| Bass  |  Mid  |Treble |  Sub  |unused |
 *    |  Read  | 0x02  | Band  | Level |unused |unused |unused |unused |unused |
 *    |        | 0x03  |Preset |Staging|unused |unused |unused |unused |unused |
 *    __________________________________________________________________________
 *
/ ------------------------- CAN MESSAGE CONTROL ----------------------*/


package com.direct.cimos.ieq;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VerticalSeekBar;

import java.io.IOException;
import java.util.List;

public class Advanced_EQ extends AppCompatActivity {

    private static final int EXTRA_SHORT_DELAY = 500; // 2 seconds
    private static final int REQUEST_TO_SAVE = 1; // 2 seconds
    public static int i = 0;
    private static final String DB_ENTER_NAME = "name"; // 2 seconds
    private static byte frame_2[];
    public String band1_b, band2_b, band3_b, band4_b, band5_b, band6_b, band7_b;
    private static int band1_i, band2_i, band3_i, band4_i, band5_i, band6_i, band7_i;
    private static byte frame_3[];
   // String address = null;
    static private final int ORIENTATION_LANDSCAPE = 0x00000002;
    static private final int ORIENTATION_PORTRAIT = 0x00000001;

    SeekBar band1, band2, band3, band4, band5, band6, band7;
    VerticalSeekBar band1_v, band2_v, band3_v, band4_v, band5_v, band6_v, band7_v;

    DBHandler db;

    int val= 12;

    Spinner preset_list;
    Shop clicked_preset;

    //class members
    String businessType[] = { "Automobile", "Food", "Computers", "Education",
            "Personal", "Travel" };
    ArrayAdapter<String> adapterBusinessType;




    // TODO
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent from_main = getIntent();
        //address = from_main.getStringExtra(iEQ.EXTRA_ADDRESS); //receive the address of the bluetooth device
        setContentView(R.layout.activity_advanced__eq);

        db = new DBHandler(this);



        band1 = (SeekBar)findViewById(R.id.seekBar7);
        band2 = (SeekBar)findViewById(R.id.seekBar8);
        band3 = (SeekBar)findViewById(R.id.seekBar9);
        band4 = (SeekBar)findViewById(R.id.seekBar10);
        band5 = (SeekBar)findViewById(R.id.seekBar11);
        band6 = (SeekBar)findViewById(R.id.seekBar12);
        band7 = (SeekBar)findViewById(R.id.seekBar13);

        band1_v = (VerticalSeekBar) findViewById(R.id.seekBar7v);
        band2_v = (VerticalSeekBar) findViewById(R.id.seekBar8v);
        band3_v = (VerticalSeekBar) findViewById(R.id.seekBar9v);
        band4_v = (VerticalSeekBar) findViewById(R.id.seekBar10v);
        band5_v = (VerticalSeekBar) findViewById(R.id.seekBar11v);
        band6_v = (VerticalSeekBar) findViewById(R.id.seekBar12v);
        band7_v = (VerticalSeekBar) findViewById(R.id.seekBar13v);

        if(getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE)
        { h_screen_layout_setup(); }
        else if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT)
        { v_screen_layout_setup(); }


        preset_list = (Spinner) findViewById(R.id.spinner);



/*

        ArrayAdapter<Shop> adapter = ArrayAdapter.createFromResource(this, R.array.Quick_Select,
                                                                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
*/

/*

        preset_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {


                clicked_preset.setName(adapter.getItemAtPosition(position).toString());
                // Showing selected spinner item
                Toast.makeText(getApplicationContext(),
                        "Selected Country : " + clicked_preset, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
*/

        show_preset_list_spinner();

        frame_2 = new byte[8];
        frame_3 = new byte[8];
        frame_2[0] = 0x02;
        frame_3[0] = 0x03;

//        try {iEQ.btSocket.getOutputStream().write(0xFFFF);}
  //      catch(IOException e) {}
    }

    // fast way to call Toast
    private void msg(String text)
    {
        final Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, EXTRA_SHORT_DELAY);
    }

    public void onSendClick(View v)
    {
//        if(iEQ.btSocket.isConnected()) {
//            try {
//                iEQ.btSocket.getOutputStream().write(frame_2);
//                iEQ.btSocket.getOutputStream().write(frame_3);
//                msg("Sent:");
//
//            } catch (IOException e) {
//                msg("Failed to send: Check your connection:");
//            }
//        }
    }

    public void returnClick(View view)
    {
        finish();
    }

    public void v_screen_layout_setup()
    {
        band1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band1_b = Integer.toString(i);
                band1_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band1_i, 1);
            }
        });
        band2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band2_b = Integer.toString(i);
                band2_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                send_single_band(band2_i, 2);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        band3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band3_b = Integer.toString(i);
                band3_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band3_i, 3);
            }
        });
        band4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band4_b = Integer.toString(i);
                band4_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band4_i, 4);
            }
        });
        band5.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band5_b = Integer.toString(i);
                band5_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band5_i, 5);
            }
        });
        band6.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band6_b = Integer.toString(i);
                band6_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band6_i, 6);
            }
        });
        band7.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band7_b = Integer.toString(i);
                band7_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band7_i, 7);
            }
        });

        v_set_progress_for_eq();
    }

    public void h_screen_layout_setup()
    {
        band1_v.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band1_b = Integer.toString(i);
                band1_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band1_i, 1);
            }
        });
        band2_v.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band2_b = Integer.toString(i);
                band2_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band2_i, 2);
            }
        });
        band3_v.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band3_b = Integer.toString(i);
                band3_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band3_i, 3);
            }
        });
        band4_v.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band4_b = Integer.toString(i);
                band4_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band4_i, 4);
            }
        });
        band5_v.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band5_b = Integer.toString(i);
                band5_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band5_i, 5);
            }
        });
        band6_v.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band6_b = Integer.toString(i);
                band6_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band6_i, 6);
            }
        });
        band7_v.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                band7_b = Integer.toString(i);
                band7_i = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                send_single_band(band7_i, 7);
            }
        });

        h_set_progress_for_eq();
    }

    public void v_set_progress_for_eq()
    {
        band1.setProgress(band1_i);
        band2.setProgress(band2_i);
        band3.setProgress(band3_i);
        band4.setProgress(band4_i);
        band5.setProgress(band5_i);
        band6.setProgress(band6_i);
        band7.setProgress(band7_i);
    }

    public void h_set_progress_for_eq()
    {
        band1_v.setProgress(band1_i);
        band2_v.setProgress(band2_i);
        band3_v.setProgress(band3_i);
        band4_v.setProgress(band4_i);
        band5_v.setProgress(band5_i);
        band6_v.setProgress(band6_i);
        band7_v.setProgress(band7_i);
    }

    public void send_single_band(int level, int band)
    {
        /*
         *    __________________________________________________________________________
         *    | CAN ID | Byte0 | Byte1 | Byte2 | Byte3 | Byte4 | Byte5 | Byte6 | Byte7 |
         *    | 0x800  | 0x01  | Fade  |Balance| Bass  |  Mid  |Treble |  Sub  |unused |
         *    | Write  | 0x02  | Band  | Level |unused |unused |unused |unused |unused |
         *    |        | 0x03  |Preset |Staging|unused |unused |unused |unused |unused |
         *    __________________________________________________________________________
         */
        frame_2[0] = 0x02;
        frame_2[1] = 0x00;      // read write bit
        frame_2[2] = (byte)(band & 0xFF);
        frame_2[3] = (byte)(level & 0xFF);

        try { iEQ.btSocket.getOutputStream().write(frame_2);}
        catch (IOException e) {}
    }



    public void read_single_band(int level, int band)
    {
        /*
         *    __________________________________________________________________________
         *    | CAN ID | Byte0 | Byte1 | Byte2 | Byte3 | Byte4 | Byte5 | Byte6 | Byte7 |
         *    | 0x800  | 0x01  | Fade  |Balance| Bass  |  Mid  |Treble |  Sub  |unused |
         *    | Write  | 0x02  | Band  | Level |unused |unused |unused |unused |unused |
         *    |        | 0x03  |Preset |Staging|unused |unused |unused |unused |unused |
         *    __________________________________________________________________________
         */


        byte buf[] = new byte[255];

        frame_2[0] = 0x02;
        frame_2[1] = 0x01;



        try
        {

            iEQ.btSocket.getInputStream().read(buf, 0, 255);
        }
        catch (IOException e) {}
    }


    public void update_spinner_list()
    {
        List<String> shops = db.getAllPresets();
        adapterBusinessType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shops);
    }

    public void  show_preset_list_spinner()
    {
        update_spinner_list();
        preset_list.setAdapter(adapterBusinessType);
    }

    public void btnSave(View view)
    {
        Intent db_saving = new Intent(Advanced_EQ.this, db_saving.class);

        startActivityForResult(db_saving, REQUEST_TO_SAVE);
       /* //startActivity(db_saving);
                  // asked the user to start bluetooth

        //db.addPreset(new Shop(1 , "Simon", "Epic", band1_b, band2_b, band3_b, band4_b, band5_b, band6_b, band7_b ));

        show_preset_list_spinner();

       *//* List<Shop> shops = db.getAllPresets();
        for (String shop : shops)
        {
            String log = "Id:   " + shop.getId() + " ,Name:     " + shop.getName() + "      ,Tag:  " + shop.getTag() + "    , Band_DATA:  " + shop.getband(1) +
                    ", "+ Integer.toString(shop.getband(2)) + ", "+ shop.getband(3) +", "+ shop.getband(4) +
                    ", "+ shop.getband(5)+ ", "+ shop.getband(5) + ", " + shop.getband(7);

            Log.d("Shop: : ", log);
        }*/
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_TO_SAVE)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                db.addPreset(new Shop(i++ , data.getStringExtra(DB_ENTER_NAME), "", band1_b, band2_b, band3_b, band4_b, band5_b, band6_b, band7_b ));
                show_preset_list_spinner();
            }
            else
            {
                return;
            }
        }

    }
}

