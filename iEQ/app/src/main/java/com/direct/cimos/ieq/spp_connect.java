/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

package com.direct.cimos.ieq;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class spp_connect extends AppCompatActivity {



    private Set<BluetoothDevice> pairedDevices;

    ListView devicelist;
    Button btnDisconnect;
    Button btnback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spp_connect);

        btnback = (Button)findViewById(R.id.button5);
        btnDisconnect = (Button)findViewById(R.id.button);

        if(iEQ.isBtConnected)
        {
            btnDisconnect.setVisibility(View.VISIBLE);
        }
        else
        {
            btnDisconnect.setVisibility(View.INVISIBLE);
        }


        devicelist = (ListView)findViewById(R.id.listview1);
        iEQ.myBluetooth = BluetoothAdapter.getDefaultAdapter();
        pairedDevicesList();

        btnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Disconnect();
            }
        });
    }

    //public void toiEQ()
    //{
    //    finish();
    //}

    private void Disconnect()
    {
        if (iEQ.btSocket!=null) //If the btSocket is busy
        {
            try{ iEQ.btSocket.close(); //close connection
                msg("Disconnected from:\n" + iEQ.connectedDeviceName );
            } catch (IOException e) { msg("Error");}

            try { iEQ.btSocket.close(); }
            catch (IOException e) {}
            iEQ.toEQ.setVisibility(View.INVISIBLE);
            iEQ.isBtConnected = false;
            iEQ.connectedDeviceName = "None";
            iEQ.btSocket = null;

        }

        finish(); //return to the first layout

    }

    private void pairedDevicesList()
    {
        pairedDevices = iEQ.myBluetooth.getBondedDevices();
        ArrayList list = new ArrayList();

        if (pairedDevices.size()>0)
        {
            for(BluetoothDevice bt : pairedDevices)
            {
                list.add(bt.getName() + "\n" + bt.getAddress()); //Get the device's name and the address
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        devicelist.setAdapter(adapter);
        devicelist.setOnItemClickListener(myListClickListener); //Method called when the device from the list is clicked

    }

    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick (AdapterView<?> av, View v, int arg2, long arg3)
        {

            String info = ((TextView) v).getText().toString();
            iEQ.address = info.substring(info.length() - 17);
            iEQ.connectedDeviceName = info.substring(0, info.length() - 17);

            Intent deviceListIntent = new Intent(spp_connect.this, iEQ.class);
            deviceListIntent.putExtra(iEQ.EXTRA_ADDRESS, iEQ.address); //this will be received at ledControl (class) Activity
            startActivity(deviceListIntent);
            //
            // finish();
        }
    };

    @Override
    public void onStop()
    {
        super.onStop();
    }

    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }

    public void back (View veiw)
    {
        finish();
    }
}
