/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

package com.direct.cimos.ieq;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import java.util.List;

public class Main2Activity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        DBHandler db = new DBHandler(this);

        // Inserting Shop/Rows
        Log.d("Insert: ", "Inserting ..");

        db.addPreset(new Shop(1 , "Simon", "Epic", "1","22","33","4","5","6","7"));

        db.addPreset(new Shop(2 , "SHawn" ,"Epic", "1","2","3","DD","5","6","7"));

        db.addPreset(new Shop(3 , "Zac", "Epic", "1","2","99","4","AA","6","7"));

        db.addPreset(new Shop(4 , "Lindon", "Epic", "FF","2","3","4","5","6","7"));

        Log.d("Reading: ", "Reading all shops..");

        /*List<Shop> shops = db.getAllPresets();

        for (Shop shop : shops)
        {
            String log = "Id:   " + shop.getId() + " ,Name:     " + shop.getName() + "      ,Tag:  " + shop.getTag() + "    , Band_DATA:  " + shop.getband(1) +
                                ", "+ shop.getband(2)+ ", "+ shop.getband(3) +", "+ shop.getband(4) +
                                ", "+ shop.getband(5)+ ", "+ shop.getband(5) + ", " + shop.getband(7);

            Log.d("Shop: : ", log);
        }
*/
    }

}




