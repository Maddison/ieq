/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

package com.direct.cimos.ieq;

import java.util.Arrays;

/**
 * Created by Cimos on 27/9/17.
 */

public class Shop
{
    private int id;
    private String name;
    private String tag;
    private static final int frame_size = 7;
    String band1;
    String band2;
    String band3;
    String band4;
    String band5;
    String band6;
    String band7;


    public Shop() {  }
    public Shop(int id, String name, String tag, String band1, String band2,
                String band3, String band4, String band5, String band6, String band7)
    {
        this.id=id;
        this.name=name;
        this.tag=tag;

        this.band1 = band1;
        this.band2 = band2;
        this.band3 = band3;
        this.band4 = band4;
        this.band5 = band5;
        this.band6 = band6;
        this.band7 = band7;

    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setband(int i, String band)
    {
        switch(i)
        {
            case 1:
                this.band1 = band;
                break;
            case 2:
                this.band2 = band;
                break;
            case 3:
                this.band3 = band;
                break;
            case 4:
                this.band4 = band;
                break;
            case 5:
                this.band5 = band;
                break;
            case 6:
                this.band6 = band;
                break;
            case 7:
                this.band7 = band;
                break;

        }
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public int getId()
    {
        return id;
    }

    public String getTag()
    {
        return tag;
    }

    public String getName()
    {
        return name;
    }

    public String getbands()
    {
        return this.band1;
    }

    public String getband(int i)
    {
        switch(i)
        {
            case 1:
                return band1;

            case 2:
                return band2;

            case 3:
                return band3;

            case 4:
                return band4;

            case 5:
                return band5;

            case 6:
                return band6;

            case 7:
                return band7;
        }
        return null;
    }
}