/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

package com.direct.cimos.ieq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class db_saving extends AppCompatActivity {

    EditText name;
    Button save_btn;
    private static final int REQUEST_TO_SAVE = 1; // 2 seconds
    private static final String DB_ENTER_NAME = "name"; // 2 seconds
    Intent from_adv_eq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db_saving);

        from_adv_eq = getIntent();


        name = (EditText)findViewById(R.id.editText);
        save_btn = (Button)findViewById(R.id.button7);


        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    request_to_save();
            }
        });
    }


    public void request_to_save()
    {
        from_adv_eq.putExtra(DB_ENTER_NAME, name.getText().toString());
        setResult(Advanced_EQ.RESULT_OK, from_adv_eq);
        finish();
    }

}
