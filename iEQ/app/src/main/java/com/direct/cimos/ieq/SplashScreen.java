/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

package com.direct.cimos.ieq;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash_screen);

        Thread mythread = new Thread()
        {
            @Override
            public void run() {
                try
                {

                    sleep(2000);


                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(getApplicationContext(), iEQ.class);
                    startActivity(intent);
                }
            }
        };
        mythread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
