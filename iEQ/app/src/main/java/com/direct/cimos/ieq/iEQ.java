/*
 * Copyright (c) 2017. This is distributed as a project only. Any use of it needs to be granted by the author.
 */

package com.direct.cimos.ieq;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Set;



public class iEQ extends AppCompatActivity implements View.OnClickListener
{

    static public BluetoothAdapter myBluetooth = null;
    static public BluetoothSocket btSocket = null;

    static public Button btnPaired;
    static public Button toEQ;
    TextView pairedDevice;

    static public String connectedDeviceName = "None";
    private static final int EXTRA_SHORT_DELAY = 300;

    static public String address;
    private static final int ENABLE_BT_REQUEST_CODE = 1;
    public static final int REQUEST_DISCOVERABLE_CODE = 2;
    public static String EXTRA_ADDRESS = "device_address";

    public static boolean isBtConnected = false;
    private ProgressDialog progress;
    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    InputStream inStream;
    OutputStream outputStream;

    // ------
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static final int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice mmDevice;
    private UUID deviceUUID;

    private Handler handler;

    String TAG = "MainActivity";
    EditText send_data;
    TextView view_data;
    private Set<BluetoothDevice> pairedDevices;

    ListView devicelist;

    // ------



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_i_eq);

        myBluetooth = BluetoothAdapter.getDefaultAdapter();

        btnPaired = (Button)findViewById(R.id.button);
        toEQ = (Button)findViewById(R.id.button4);
        pairedDevice = (TextView)findViewById(R.id.textView6);

        //if(isBtConnected) { showtoEQ_but(); }
        //else { hidetoEQ_but(); }
        showtoEQ_but();

        if (myBluetooth == null)
        {
            Toast.makeText(getApplicationContext(), "This device doesn’t support Bluetooth", Toast.LENGTH_SHORT).show();
        } else
        {
            if (!myBluetooth.isEnabled())
            {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, ENABLE_BT_REQUEST_CODE);           // asked the user to start bluetooth
                Toast.makeText(getApplicationContext(), "Enabling Bluetooth!", Toast.LENGTH_SHORT).show();
            }
        }

        btnPaired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                toSPPConnect();
            }
        });

        //Spinner spinner = (Spinner) findViewById(R.id.spinner);
        //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Quick_Select, android.R.layout.simple_spinner_item);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //spinner.setAdapter(adapter);

        Intent sppDevice = getIntent();
        if (sppDevice.hasExtra(iEQ.EXTRA_ADDRESS))
        {
            address = sppDevice.getStringExtra(iEQ.EXTRA_ADDRESS); //receive the address of the bluetooth device
            new ConnectBT().execute(); //Call the class to connect
        }



        // ---- connected





        //send_data =(EditText) findViewById(R.id.editText);
//        view_data = (TextView) findViewById(R.id.textView);

        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new
                    Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

    }

    @Override
    public void onResume()
    {
        if(isBtConnected) { toEQ.setVisibility(View.VISIBLE); }

        getLocalBluetoothName();
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (btSocket.isConnected())
        {
            try { btSocket.close(); }
            catch (IOException e) {}
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == ENABLE_BT_REQUEST_CODE)
        {
            if (resultCode == Activity.RESULT_OK)
            {
            }
            else if(resultCode == RESULT_CANCELED) {

                //...then display this alternative toast.//
                Toast.makeText(getApplicationContext(), "An error occurred while attempting to enable Bluetooth",
                        Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Bluetooth request has been denied!",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        }

    }

    public void toBasicEQ(View view)
    {
        if (!isBtConnected) {msg("Connect a Bluetooth device first:"); return;}
        Intent basicEQ_intent = new Intent(this, Basic_EQ.class);
        basicEQ_intent.putExtra(EXTRA_ADDRESS, address); //this will be received at ledControl (class) Activity

        startActivity(basicEQ_intent);
    }

    public void toAdvancedEQ(View view)
    {
        if (!isBtConnected) {msg("Connect a Bluetooth device first:"); return;}
        Intent myIntent = new Intent(this, Advanced_EQ.class);
        startActivity(myIntent);
    }

    public void toSPPConnect()
    {
        Intent spp_connect_intent = new Intent(iEQ.this, spp_connect.class);
        startActivity(spp_connect_intent);
    }

    public void getLocalBluetoothName()
    {
        pairedDevice.setText("Pairded Device: " + connectedDeviceName);
    }

    public void onClick(View v)
    {

    }


    public void showtoEQ_but()
    {
        toEQ.setVisibility(View.VISIBLE);
    }
    public void hidetoEQ_but()
    {
        toEQ.setVisibility(View.INVISIBLE);
    }






    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(iEQ.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection

                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection

                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                msg("Connection Failed.");
                connectedDeviceName = "None";
                finish();
            }
            else
            {
                msg("Connected.");
                isBtConnected = true;
                //Intent connected_to_device = new Intent(iEQ.this, Basic_EQ.class);
                //startActivity(connected_to_device);


            }
            progress.dismiss();
        }
    }



    private void msg(String text)
    {
        final Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, EXTRA_SHORT_DELAY);
    }
}




/*







    // ---- connected thread ----

    public void pairDevice(View v) {

        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        Log.e("MAinActivity", "" + pairedDevices.size());
        if (pairedDevices.size() > 0) {
            Object[] devices = pairedDevices.toArray();
            BluetoothDevice device = (BluetoothDevice) devices[0];
            //ParcelUuid[] uuid = device.getUuids();
            Log.e("MAinActivity", "" + device);
            //Log.e("MAinActivity", "" + uuid)

            ConnectThread connect = new ConnectThread(device, MY_UUID_INSECURE);
            connect.start();

        }
    }



    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;
        private BluetoothSocket btSocket = null;

        public ConnectThread(BluetoothDevice device, UUID uuid) {
            Log.d(TAG, "ConnectThread: started.");
            mmDevice = device;
            deviceUUID = uuid;
        }

        public void run() {
            BluetoothSocket tmp = null;
            Log.i(TAG, "RUN mConnectThread ");

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {

                tmp = mmDevice.createRfcommSocketToServiceRecord(MY_UUID_INSECURE);
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread: Could not create InsecureRfcommSocket " + e.getMessage());
            }

            mmSocket = tmp;

            // Make a connection to the BluetoothSocket

            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();

            } catch (IOException e) {
                // Close the socket
                try {
                    mmSocket.close();
                    Log.d(TAG, "run: Closed Socket.");
                } catch (IOException e1) {
                    Log.e(TAG, "mConnectThread: run: Unable to close connection in socket " + e1.getMessage());
                }
                Log.d(TAG, "run: ConnectThread: Could not connect to UUID: " + MY_UUID_INSECURE);
            }

            //will talk about this in the 3rd video
            connected(mmSocket);
        }

        public void cancel() {
            try {
                Log.d(TAG, "cancel: Closing Client Socket.");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "cancel: close() of mmSocket in Connectthread failed. " + e.getMessage());
            }
        }
    }

    private void connected(BluetoothSocket mmSocket) {
        Log.d(TAG, "connected: Starting.");

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.start();
    }

    private class ConnectedThread extends Thread {

        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread: Starting.");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;



            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run(){
            byte[] buffer = new byte[1024];  // buffer store for the stream

            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                // Read from the InputStream
                try {
                    bytes = mmInStream.read(buffer);
                    final String incomingMessage = new String(buffer, 0, bytes);
                    Log.d(TAG, "InputStream: " + incomingMessage);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            view_data.setText(incomingMessage);
                        }
                    });


                } catch (IOException e) {
                    Log.e(TAG, "write: Error reading Input Stream. " + e.getMessage() );
                    break;
                }
            }
        }


        public void write(byte[] bytes) {
            String text = new String(bytes, Charset.defaultCharset());
            Log.d(TAG, "write: Writing to outputstream: " + text);
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
                Log.e(TAG, "write: Error writing to output stream. " + e.getMessage() );
            }
        }

        /* Call this from the main activity to shutdown the connection
public void cancel() {
    try {
        mmSocket.close();
    } catch (IOException e) { }
}
}


public void SendMessage(View v) {
        byte[] bytes = send_data.getText().toString().getBytes(Charset.defaultCharset());
        mConnectedThread.write(bytes);
        }

private void Disconnect()
        {
        if (iEQ.btSocket!=null) //If the btSocket is busy
        {
        try{ iEQ.btSocket.close(); //close connection
        msg("Disconnected from:\n" + iEQ.connectedDeviceName );
        } catch (IOException e) { msg("Error");}

        try { iEQ.btSocket.close(); }
        catch (IOException e) {}
        iEQ.toEQ.setVisibility(View.INVISIBLE);
        iEQ.isBtConnected = false;
        iEQ.connectedDeviceName = "None";
        iEQ.btSocket = null;

        }

        finish(); //return to the first layout

        }

private void pairedDevicesList()
        {
        pairedDevices = iEQ.myBluetooth.getBondedDevices();
        ArrayList list = new ArrayList();

        if (pairedDevices.size()>0)
        {
        for(BluetoothDevice bt : pairedDevices)
        {
        list.add(bt.getName() + "\n" + bt.getAddress()); //Get the device's name and the address
        }
        }
        else
        {
        Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        devicelist.setAdapter(adapter);
        devicelist.setOnItemClickListener(myListClickListener); //Method called when the device from the list is clicked

        }

private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener()
        {
public void onItemClick (AdapterView<?> av, View v, int arg2, long arg3)
        {

        String info = ((TextView) v).getText().toString();
        iEQ.address = info.substring(info.length() - 17);
        iEQ.connectedDeviceName = info.substring(0, info.length() - 17);

        //Intent deviceListIntent = new Intent(spp_connect.this, iEQ.class);
        //deviceListIntent.putExtra(iEQ.EXTRA_ADDRESS, iEQ.address); //this will be received at ledControl (class) Activity
        //startActivity(deviceListIntent);
        finish();
        }
        };



@Override
public void onStop()
        {
        super.onStop();
        }

private void msg(String s)
        {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
        }




    public void Start_Server(View view) {

        AcceptThread accept = new AcceptThread();
        accept.start();

    }

    private class AcceptThread extends Thread {

        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread(){
            BluetoothServerSocket tmp = null ;

            // Create a new listening server socket
            try{
                tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("appname", MY_UUID_INSECURE);

                Log.d(TAG, "AcceptThread: Setting up Server using: " + MY_UUID_INSECURE);
            }catch (IOException e){
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage() );
            }

            mmServerSocket = tmp;
        }

        public void run(){
            Log.d(TAG, "run: AcceptThread Running.");

            BluetoothSocket socket = null;

            try{
                // This is a blocking call and will only return on a
                // successful connection or an exception
                Log.d(TAG, "run: RFCOM server socket start.....");

                socket = mmServerSocket.accept();

                Log.d(TAG, "run: RFCOM server socket accepted connection.");

            }catch (IOException e){
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage() );
            }

            //talk about this is in the 3rd
            if(socket != null){
                connected(socket);
            }

            Log.i(TAG, "END mAcceptThread ");
        }

        public void cancel() {
            Log.d(TAG, "cancel: Canceling AcceptThread.");
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "cancel: Close of AcceptThread ServerSocket failed. " + e.getMessage() );
            }
        }

    }
 */
