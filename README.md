# Build Instructions

### Windows

... ... ...

 

# Command line instructions

### Git global setup

```
git config --global user.name "You"
git config --global user.email "YourEmail@Domain.com"
```


### Create a new repository

```
git clone 
cd myproject
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```


### Existing folder

```
cd existing_folder
git init
git remote add origin 
git add .
git commit
git push -u origin master
```


### Existing Git repository

```
cd existing_repo
git remote add origin 
git push -u origin --all
git push -u origin --tags
```




### Source's used in project

```
http://www.instructables.com/id/Android-Bluetooth-Control-LED-Part-2/
https://stackoverflow.com/questions/7489454/activity-crashes-on-setadapterarrayadapter
https://github.com/android/platform_frameworks_base/tree/master/core/res/res/layout
https://stackoverflow.com/questions/3663745/what-is-android-r-layout-simple-list-item-1
https://stackoverflow.com/questions/5836662/extending-from-two-classes
